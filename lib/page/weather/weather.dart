import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/provider/weatherProvider.dart';
import 'package:pruebas/utils/Format.dart';
import 'package:pruebas/widget/appBarCustom.dart';
import 'package:pruebas/widget/titleApp.dart';
import 'package:provider/provider.dart';

class Weather extends StatefulWidget {
  @override
  _WeatherState createState() => _WeatherState();
}

class _WeatherState extends State<Weather> {
  double degrees = 0.0;
  String country = '';
  String city = '';
  String date = '';
  String description = '';

  @override
  void initState() {
    super.initState();
    Provider.of<WeatherProvider>(context, listen: false).getZone();
  }

  @override
  Widget build(BuildContext context) {
    WeatherProvider weatherProvider = Provider.of<WeatherProvider>(context);
    degrees = weatherProvider.weatherSelect.consolidatedWeather[0].theTemp;
    country = weatherProvider.weatherSelect.parent.title;
    city = weatherProvider.weatherSelect.title;
    date = "${weatherProvider.weatherSelect.consolidatedWeather[0].applicableDate.day}-${weatherProvider.weatherSelect.consolidatedWeather[0].applicableDate.month}-${weatherProvider.weatherSelect.consolidatedWeather[0].applicableDate.year}";
    description = weatherProvider.weatherSelect.consolidatedWeather[0].weatherStateName;
    return Scaffold(
      appBar: AppBarCustom(
        label: "El Clima",
      ),
      body: weatherProvider.isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(bottom: 40),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TitleApp(
                            label:
                                "${degrees.toStringAsFixed(0)}º C ${iconWeather(degrees)}",
                            size: 50,
                          ),
                          TitleApp(
                            label: '$country - $city',
                            size: 30,
                          ),
                          TitleApp(
                            label: date,
                            size: 30,
                          ),
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 40),
                    alignment: Alignment.centerRight,
                    child: Text(
                      description,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 40,
                        color: Colors.black,
                      ),
                      textAlign: TextAlign.end,
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
