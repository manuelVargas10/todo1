import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/provider/loginPorvider.dart';
import 'package:pruebas/utils/validation.dart';
import 'package:pruebas/widget/button.dart';
import 'package:pruebas/widget/input.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with Validation {
  late final GlobalKey<FormState> _formKey;
  late TextEditingController emailController;
  late TextEditingController passwordController;
  bool validation = false;
  late Size size;

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    _formKey = GlobalKey<FormState>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    LoginProvider loginProvider = Provider.of<LoginProvider>(context);
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top: 130, bottom: 30),
                child: Image(
                  image: AssetImage('assets/image/todo1.webp'),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 5),
                child: Input(
                  hintText: 'Correo electrónico',
                  controller: emailController,
                  validator: _validationEmail,
                  iconData: Icons.person_outline,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 5),
                child: Input(
                  obscureText: true,
                  hintText: 'Contraseña',
                  controller: passwordController,
                  validator: _validationPassword,
                  iconData: Icons.vpn_key_outlined,
                ),
              ),
              loginProvider.isLoading
                  ? Container(
                margin: EdgeInsets.only(top: 10),
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : Button(label: 'Inicio', handled: _sigIn)
            ],
          ),
        ),
      ),
    );
  }

  _sigIn() async {
    LoginProvider loginProvider =
        Provider.of<LoginProvider>(context, listen: false);
    loginProvider.isLoading = true;
    if (_formKey.currentState!.validate()) {
      validation = await loginProvider.authentication(
          email: emailController.text,
          password: passwordController.text,
          context: context);
      if (validation) Navigator.of(context).pushReplacementNamed('/home');
    }
    loginProvider.isLoading = false;
  }

  _validationEmail(value) {
    if (value.isEmpty)
      return 'Este campo no puede ser vacío';
    else if (!emailValidation(value))
      return 'Formato incorrecto';
    else
      return null;
  }

  _validationPassword(value) {
    if (value.isEmpty)
      return 'Este campo no puede ser vacío';
    else
      return null;
  }
}
