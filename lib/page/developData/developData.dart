import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/widget/appBarCustom.dart';
import 'package:pruebas/widget/textAplication.dart';
import 'package:pruebas/widget/titleApp.dart';

class DevelopData extends StatefulWidget {
  @override
  _DevelopDataState createState() => _DevelopDataState();
}

class _DevelopDataState extends State<DevelopData> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(label: 'información'),
      body: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(2, 3),
                  blurRadius: 2,
                  spreadRadius: 0)
            ],),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              height: 100,
              width: 100,
              margin: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
              decoration: BoxDecoration(
                  color: Colors.green,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(2, 3),
                        blurRadius: 2,
                        spreadRadius: 0)
                  ],
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(
                        "https://lh3.googleusercontent.com/-KeBY0B65fjc/WsUbA6zLTpI/AAAAAAAAa7M/mjF7NCdobJs60PI-_VO9YnEHEVRP4bi4wCEwYBhgLKtMDAL1OcqxmyTtOb4_0m8w4wkAaTxar1H8jzkQRScQJ7WRYe00YP74mecAXO76uoHk4xMgvtwzKv0CDojT71vhzfqKOALt4a1P1FKJY_UM0bKeag8JlXOfyauVkWc9r32fYq0WtpMVJrp9DzJPKaVhJDLsfp6yFmV0x7-byNbc56DaDCx_Qxl6P3qhipf86qlv5g6UryBjKwbj5hSA6tlQWQT1VBHwd8Ll4aA77d74VInNKDHrAE29Pwo5f9jKAJy-OHr1fVRNn9mWV4Sr2m1jn3f89VYmslNfEM2nZYbwnE6WqizKis2jCsvn7HQAg6dMKRGTOHGuggcVFDGLCLYAZTU8nL7sPg3cOyKdkCNJqqUGEcIZO4mv9q35zUGsR3Op2FSvdsrWbRL25Vo3eyhaSKzd4uUVp-LFUtJPYI5C-KQvfR21C9b0naA9hk_U8EgzBZu8W5fZxWu_YjNZKX-qEhUB8QWFPPynGdULRcRTprnYiUi0CJRmuFD_Wk0uDKgynQfsZ1heGHJRysr7HYq2jSnsPe8nb6cvsVLmXXzjCN3iueC9mPAy8m9QpOqa2iSK9yuY12bSy0SCvrP0WxxP9VmrsUVCFC2-TpnTDaaf_h4oKV1AwnZDPhQY/w140-h140-p/29695358_2143761805857132_4053128560318896665_n.jpg"),
                  )),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
              child: TitleApp(label: 'Jeison Manuel Vargas Vargas',)
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
              child: TextApp(
                label: 'Ingeniero de software graduado de la Universidad Fundación Escuela Tecnológica Jesús Oviedo Pérez, con experiencia en desarrollo de aplicaciones Móviles, Back-end y plataformas Web, bases de datos SQL y no relacionales. \n\n He participado en planeación he implementación de distintos proyectos, como desarrollador de software Front-end y Back-end donde he demostrado un alto nivel de compromiso y responsabilidad, capacidad de trabajo bajo presión y en equipo y aporte creativo para la solucione de problemas en áreas técnicas. ',
              ),
            )
          ],
        ),
      ),
    );
  }
}
