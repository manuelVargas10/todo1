import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/provider/loginPorvider.dart';
import 'package:pruebas/utils/preferences.dart';
import 'package:provider/provider.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  final prefs = PreferenceUser();

  @override
  void initState() {
    super.initState();
    validateCredentials();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Center(
          child: Image(
            image: AssetImage('assets/image/todo1.webp'),
          ),
        ),
      ),
    );
  }

  validateCredentials() async {
    Map user = await prefs.getUser();
    if (user['email'] != null) {
      await Provider.of<LoginProvider>(context, listen: false).authentication(email: user['email'], password: user['password'], context: context);
      Navigator.of(context).pushReplacementNamed('/home');
    } else
      Navigator.of(context).pushReplacementNamed('/login');
  }
}
