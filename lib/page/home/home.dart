import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/widget/iconButton.dart';
import 'package:pruebas/provider/userProvider.dart';
import 'package:pruebas/widget/textAplication.dart';
import 'package:pruebas/provider/accountProvider.dart';
import 'package:pruebas/page/home/widget/bottomCollapse.dart';
import 'package:pruebas/page/home/widget/headerCollapse.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  void initState() {
    super.initState();
    final AccountProvider accountProvider = Provider.of<AccountProvider>(context, listen: false);
    final UserProvider userProvider = Provider.of<UserProvider>(context, listen: false);
    accountProvider.getPrincipalAccount(id: userProvider.userData.id, context: context);
  }

  @override
  Widget build(BuildContext context) {
    final AccountProvider accountProvider = Provider.of<AccountProvider>(context);
    return Scaffold(
      body: HeaderCollapse(
        rating: 2.3,
        handledRating: (e) {},
        body: Column(
          children: [
            BottomCollapse(),
            Container(
              margin: EdgeInsets.symmetric(vertical: 60),
              child: TextApp(label: '¿Que quieres hacer hoy?'),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              padding: EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(3, 2),
                        color: Colors.black12,
                        spreadRadius: 1,
                        blurRadius: 7)
                  ]),
              child: Row(
                children: [
                  Expanded(
                      child: IconButtonWidget(
                    label: 'Cuentas Ahorros',
                    icon: Icons.credit_card,
                    handledButton: () => goToListCount(
                        title: 'Cuentas de Ahorros', isSaving: true),
                  )),
                  Expanded(
                      child: IconButtonWidget(
                    label: 'Cuentas Corriente',
                    icon: Icons.credit_card,
                    handledButton: () => goToListCount(
                        title: 'Cuentas Corriente', isSaving: false),
                  )),
                  Expanded(
                      child: IconButtonWidget(
                    label: 'Clima',
                    icon: Icons.wb_sunny_outlined,
                    handledButton: () => Navigator.of(context).pushNamed('/weather'),
                  )),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => accountProvider.handledButtonQR(context, accountProvider.principalAccount),
        child: const Icon(Icons.qr_code_outlined, size: 40),
        backgroundColor: Colors.blue[900],
      ),
    );
  }

  Future<void> goToListCount({required bool isSaving, required String title}) async {
    final accountProvider = Provider.of<AccountProvider>(context, listen: false);
    accountProvider.isSaving = isSaving;
    accountProvider.titleView = title;
    Navigator.of(context).pushNamed('/listCount');
  }
}
