import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/page/home/widget/titleCollapse.dart';
import 'package:pruebas/provider/userProvider.dart';
import 'package:pruebas/widget/titleApp.dart';

class HeaderCollapse extends StatelessWidget {
  final Widget body;
  final double rating;
  final Function(double) handledRating;

  HeaderCollapse(
      {required this.body, required this.rating, required this.handledRating});

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    return DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                expandedHeight: 130.0,
                floating: false,
                pinned: false,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  titlePadding: EdgeInsets.only(bottom: 25),
                  background: Container(
                    color: Colors.white,
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      children: [
                        TitleCollapse(),
                        Container(
                            margin: EdgeInsets.only(top: 40),
                            alignment: Alignment.centerLeft,
                            child: TitleApp(
                                label:
                                    'Hola ${userProvider.userData.name.split(' ')[0]}'))
                      ],
                    ),
                  ),
                ),
              )
            ];
          },
          body: Center(
            child: body,
          ),
        ));
  }
}
