import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/widget/textAplication.dart';

class IconAndLabel extends StatelessWidget {
  final IconData icon;
  final String label;
  final bool left;

  const IconAndLabel({required this.icon, required this.label, this.left = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: left ? MainAxisAlignment.start: MainAxisAlignment.end,
        children: [
          left ? Icon(icon, color: Colors.grey[700], size: 20) : Container(),
          TextApp(label: label, size: 13,),
          !left ? Icon(icon, color: Colors.grey[700], size: 20,) : Container(),
        ],
      ),
    );
  }
}
