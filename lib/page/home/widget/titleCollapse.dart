import 'package:pruebas/page/home/widget/iconAndLabel.dart';
import 'package:pruebas/provider/loginPorvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TitleCollapse extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final LoginProvider loginProvider = Provider.of<LoginProvider>(context);
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Row(
        children: [
          Expanded(
              child: GestureDetector(
                onTap: () => Navigator.of(context).pushNamed('/developData'),
                child: IconAndLabel(
                  label: 'Información',
                  icon: Icons.error_outline,
                  left: true,
                ),
              )),
          Expanded(
              child: Image(
                image: AssetImage('assets/image/todo1.webp'),
              )),
          Expanded(
              child: GestureDetector(
                onTap: (){
                  loginProvider.signOff(context);
                },
                child: IconAndLabel(
                  label: 'Cerrar cesión',
                  icon: Icons.logout,
                ),
              )),
        ],
      ),
    );
  }
}
