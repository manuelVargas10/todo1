import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/utils/Format.dart';
import 'package:pruebas/utils/constans.dart';
import 'package:pruebas/widget/textAplication.dart';
import 'package:pruebas/provider/accountProvider.dart';
import 'package:pruebas/widget/titleApp.dart';

class BottomCollapse extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final AccountProvider accountProvider = Provider.of<AccountProvider>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 30,
          color: Colors.white,
        ),
        Stack(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              alignment: Alignment.centerRight,
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(1, 9),
                        color: Colors.black12,
                        spreadRadius: 1,
                        blurRadius: 7)
                  ],
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10))),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    TitleApp(label: "Cuenta Principal"),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextApp(label: accountProvider.principalAccount.accountType),
                        TextApp(label: " - "),
                        TextApp(label: numberFormat(accountProvider.principalAccount.amount))
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 35, left: 20),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: colorGrey),
                  borderRadius: BorderRadius.circular(30)),
              child: TextApp(label: accountFormat(accountProvider.principalAccount.accountNumber)),
            )
          ],
        )
      ],
    );
  }
}
