import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/model/accountModel.dart';
import 'package:pruebas/model/userModel.dart';
import 'package:pruebas/provider/accountProvider.dart';
import 'package:pruebas/provider/userProvider.dart';
import 'package:pruebas/utils/Format.dart';
import 'package:pruebas/utils/validation.dart';
import 'package:pruebas/widget/appBarCustom.dart';
import 'package:pruebas/widget/button.dart';
import 'package:pruebas/widget/input.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/widget/titleApp.dart';

class Transfer extends StatefulWidget {
  @override
  _TransferState createState() => _TransferState();
}

class _TransferState extends State<Transfer> {
  late final GlobalKey<FormState> _formKey;
  late TextEditingController accountController;
  late TextEditingController amountController;
  late TextEditingController userController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    accountController = TextEditingController();
    amountController = TextEditingController();
    userController = TextEditingController();
    getDataTransfer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustom(label: "Transferencia"),
      body:  Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  alignment: Alignment.centerLeft,
                  child: TitleApp(label: "Producto destino"),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5),
                  child: Input(
                    readOnly: true,
                    hintText: 'Usuario de destino',
                    controller: userController,
                    validator: (e)=> null,
                    iconData: Icons.person_outline,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5),
                  child: Input(
                    hintText: 'Ahorros',
                    controller: accountController,
                    readOnly: true,
                    validator: (e) => null,
                    iconData: Icons.credit_card,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5),
                  child: Input(
                    hintText: 'Valor a enviar',
                    controller: amountController,
                    validator: _validationAmount,
                    iconData: Icons.monetization_on_outlined,
                  ),
                ),
                Button(
                    label: 'ENVIAR DINERO',
                    handled: () async => handledTransfer())
              ],
            ),
          ),
        ),
      )
    );
  }

  getDataTransfer() async {
    final accountProvider =
    Provider.of<AccountProvider>(context, listen: false);
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    AccountModel accountModel = await accountProvider.getAccountByIdProvider(accountProvider.idAccountToTransfer);
    UserModel userModel =
    await userProvider.getUserByIdProvider(accountModel.userId);
    accountController.text = accountFormat(accountModel.accountNumber);
    userController.text = userModel.name;
  }

  handledTransfer() async {
    if (_formKey.currentState!.validate()){
      final accountProvider = Provider.of<AccountProvider>(context, listen: false);
      await accountProvider.transferMoneyProvider(
          accountGet: accountProvider.accountToTransfer,
          accountSend: accountProvider.accountModel,
          value: int.parse(amountController.text)
      );
      Navigator.of(context).pop();
    }
  }

  _validationAmount(value) {
    final accountProvider =
    Provider.of<AccountProvider>(context, listen: false);
    if (value.isEmpty)
      return 'Este campo no puede ser vacío';
    else if (!Validation().numberValidation(value))
      return 'Formato incorrecto';
    else if (int.parse(value) <= 0)
      return 'El valor no puede ser menor o igual a cero';
    else if (accountProvider.accountModel.amount <= int.parse(value))
      return 'No cuentas con esta cantidad para enviar';
    else
      return null;
  }
}
