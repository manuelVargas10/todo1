import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/model/accountModel.dart';
import 'package:pruebas/widget/appBarCustom.dart';
import 'package:pruebas/provider/userProvider.dart';
import 'package:pruebas/provider/accountProvider.dart';
import 'package:pruebas/page/listCount/cardAccount.dart';

class ListCount extends StatefulWidget {
  @override
  _ListCountState createState() => _ListCountState();
}

class _ListCountState extends State<ListCount> {
  @override
  void initState() {
    getListAccount();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final AccountProvider accountProvider =
        Provider.of<AccountProvider>(context);
    return Scaffold(
      appBar: AppBarCustom(label: accountProvider.titleView),
      body: accountProvider.listAccountModel.length > 0
          ? SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                      shrinkWrap: true,
                      itemCount: accountProvider.listAccountModel.length,
                      itemBuilder: (BuildContext context, int index) {
                        AccountModel account =
                            accountProvider.listAccountModel[index];
                        return CardAccount(account: account);
                      })
                ],
              ),
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  getListAccount() {
    final accountProvider = Provider.of<AccountProvider>(context, listen: false);
    final UserProvider userProvider = Provider.of<UserProvider>(context, listen: false);
    if (accountProvider.isSaving)
      accountProvider.getListAccountSaving(id: userProvider.userData.id, context: context);
    else accountProvider.getListAccountCurrent(id: userProvider.userData.id, context: context);
  }
}
