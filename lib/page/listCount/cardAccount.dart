import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:pruebas/utils/Format.dart';
import 'package:pruebas/widget/buttonQR.dart';
import 'package:pruebas/widget/titleApp.dart';
import 'package:pruebas/model/accountModel.dart';
import 'package:pruebas/widget/textAplication.dart';
import 'package:pruebas/provider/accountProvider.dart';

class CardAccount extends StatelessWidget {

  final AccountModel account;

  const CardAccount({required this.account});

  @override
  Widget build(BuildContext context) {
    final AccountProvider accountProvider = Provider.of<AccountProvider>(context);
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: 10, vertical: 10),
      padding:
      EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
                offset: Offset(3, 2),
                color: Colors.black12,
                spreadRadius: 1,
                blurRadius: 2)
          ]),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 2),
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: TitleApp(
                      label: accountProvider.titleView,
                      size: 15,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.centerRight,
                    child: Icon(Icons.check_circle, color: account.isPrincipal ? Colors.green : Colors.transparent,)
                  ),
                )
              ],
            )
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 2),
            alignment: Alignment.centerLeft,
            child: TitleApp(
              label: accountProvider.titleView.split(" ")[accountProvider.titleView.split(" ").length - 1],
              size: 13,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 2),
            alignment: Alignment.centerLeft,
            child: TextApp(
              label: accountFormat(account.accountNumber),
              size: 13,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 2),
            alignment: Alignment.centerRight,
            child: TitleApp(
              label: 'Saldo disponible',
              size: 13,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 2),
            alignment: Alignment.centerRight,
            child: TextApp(
              label: numberFormat(account.amount),
              size: 13,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 5),
            child: ButtonQR(
                handledButton: () => accountProvider.handledButtonQR(context, account)),
          ),
        ],
      ),
    );
  }
}
