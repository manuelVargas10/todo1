import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:pruebas/provider/accountProvider.dart';
import 'package:provider/provider.dart';

class AccountServices {
  Future<void> getListAccountSaving(
      {required String id, required BuildContext context}) async {
    FirebaseFirestore.instance
        .collection('Accounts')
        .where('userId', isEqualTo: id)
        .where('accountType', isEqualTo: "ahorros")
        .snapshots()
        .listen((QuerySnapshot event) {
          List<DocumentSnapshot> data = event.docs;
          Provider.of<AccountProvider>(context, listen: false).getListAccountSavingProvider(data);
    });
  }

  Future<void> getListAccountCurrent(
      {required String id, required BuildContext context}) async {
    FirebaseFirestore.instance
        .collection('Accounts')
        .where('userId', isEqualTo: id)
        .where('accountType', isEqualTo: "corriente")
        .snapshots()
        .listen((QuerySnapshot event) {
      List<DocumentSnapshot> data = event.docs;
      Provider.of<AccountProvider>(context, listen: false)
          .getListAccountCurrentProvider(data);
    });
  }

  Future<void> getPrincipalAccount(
      {required String id, required BuildContext context}) async {
    FirebaseFirestore.instance
        .collection('Accounts')
        .where('userId', isEqualTo: id)
        .where('isPrincipal', isEqualTo: true)
        .snapshots()
        .listen((QuerySnapshot event) {
      List<DocumentSnapshot> data = event.docs;
      Provider.of<AccountProvider>(context, listen: false)
          .getPrincipalAccountProvider(data);
    });
  }

  Future<void> transferMoney(
      Map<String, dynamic> accountModel, String id) async {
    FirebaseFirestore.instance
        .collection('Accounts')
        .doc(id)
        .update(accountModel);
  }

  Future<DocumentSnapshot> getAccountById(String id) async =>
      await FirebaseFirestore.instance.collection('Accounts').doc(id).get();
}
