import 'package:pruebas/model/responseDb.dart';
import 'package:pruebas/model/weatherModel.dart';
import 'package:pruebas/model/zoneModel.dart';
import 'package:pruebas/service/abstractApi.dart';
import 'package:pruebas/utils/constans.dart';

class WeatherServices extends AbstractApi {
  Future<List<ZoneModel>> getListZone(double lat, double long) async {
    final ResponseDB response = await readData(urlSpecific: "$URL/location/search/?lattlong=$lat,$long");
    if (response.isSuccess) {
      List dataZone = response.result;
      List<ZoneModel> listZone =  dataZone.map((e) => ZoneModel.fromJson(e)).toList();
      return listZone;
    }
    return [];
  }

  Future getWeather(int idZone) async {
    final ResponseDB response = await readData(urlSpecific: "$URL/location/$idZone");
    if (response.isSuccess) {
      Map<String, dynamic> dataWeather = response.result;
      WeatherModel weatherData =   WeatherModel.fromJson(dataWeather);
      return weatherData;
    }
    return null;
  }
}
