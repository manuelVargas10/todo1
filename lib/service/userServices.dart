import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserServices {
  authenticationUser({required String email, required String password}) async =>
      await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);

  Future<DocumentSnapshot>getUserById(String id) async =>
      await FirebaseFirestore.instance.collection('Users').doc(id).get();
}
