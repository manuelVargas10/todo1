import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:pruebas/model/userModel.dart';
import 'package:pruebas/provider/userProvider.dart';
import 'package:pruebas/service/userServices.dart';
import 'package:pruebas/utils/Format.dart';
import 'package:pruebas/utils/preferences.dart';
import 'package:pruebas/utils/userAlerDialog.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class LoginProvider extends UserServices with ChangeNotifier {
  late bool _isLoading = false;

  bool get isLoading => _isLoading;

  set isLoading(bool data) {
    _isLoading = data;
    notifyListeners();
  }

  Future<bool> authentication(
      {required String email,
      required String password,
      required BuildContext context}) async {
    try {
      UserCredential userCredential =
          await authenticationUser(email: email, password: password);
      DocumentSnapshot documentSnapshot =
          await getUserById(userCredential.user!.uid);
      Map<String, dynamic> data = parseObjectToMap(documentSnapshot.data());
      PreferenceUser().setUser({'email': email, 'password': password});
      final userProvider = Provider.of<UserProvider>(context, listen: false);
      userProvider.userData = UserModel.fromJson(data, documentSnapshot.id);
      print(userProvider.userData);
      return true;
    } catch (e) {
      dynamic message = e;
      useAlertDialog(
          context: context, title: 'Algo salio mal', message: message.message);
      return false;
    }
  }

  signOff(BuildContext context) {
    PreferenceUser().deleteUser();
    Navigator.of(context).pushReplacementNamed('/login');
  }
}
