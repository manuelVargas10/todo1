import 'package:flutter/material.dart';
import 'package:pruebas/model/weatherModel.dart';
import 'package:pruebas/model/zoneModel.dart';
import 'package:pruebas/service/weatherServices.dart';

class WeatherProvider extends WeatherServices with ChangeNotifier {
  late ZoneModel _zoneSelect;
  late WeatherModel _weatherSelect;
  late bool _isLoading = false;

  ZoneModel get zoneSelect => _zoneSelect;

  WeatherModel get weatherSelect => _weatherSelect;

  bool get isLoading => _isLoading;

  set zoneSelect(ZoneModel data) {
    _zoneSelect = data;
    notifyListeners();
  }

  set isLoading(bool data) {
    _isLoading = data;
    notifyListeners();
  }

  set weatherSelect(WeatherModel data) {
    _weatherSelect = data;
    notifyListeners();
  }

  Future<void> getZone() async {
    isLoading = true;
    List<ZoneModel> listZone = await getListZone(1.8564229, -76.0452291);
    zoneSelect = listZone.firstWhere((ZoneModel element) => element.woeid == 368148);
    getWeatherProvider();
  }

  Future<void> getWeatherProvider() async {
    weatherSelect = await getWeather(zoneSelect.woeid);
    isLoading = false;
  }

  @override
  void dispose() {
    super.dispose();
    isLoading = false;
  }
}
