import 'package:flutter/material.dart';
import 'package:pruebas/utils/Format.dart';
import 'package:pruebas/widget/titleApp.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:pruebas/widget/generateQR.dart';
import 'package:pruebas/model/accountModel.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:pruebas/widget/textAplication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:pruebas/service/accountServices.dart';

class AccountProvider extends AccountServices with ChangeNotifier {
  late bool _isSaving = false;
  late String _titleView = '';
  late AccountModel _accountModel;
  late String _idAccountToTransfer = '';
  late AccountModel _principalAccount;
  late AccountModel _accountToTransfer;
  late List<AccountModel> _listAccountModel = [];

  bool get isSaving => _isSaving;
  String get titleView => _titleView;
  AccountModel get accountModel => _accountModel;
  String get idAccountToTransfer => _idAccountToTransfer;
  AccountModel get principalAccount => _principalAccount;
  AccountModel get accountToTransfer => _accountToTransfer;
  List<AccountModel> get listAccountModel => _listAccountModel;

  set listAccountModel(List<AccountModel> data) {
    _listAccountModel = data;
    notifyListeners();
  }

  set accountModel(AccountModel data) {
    _accountModel = data;
    notifyListeners();
  }

  set principalAccount(AccountModel data) {
    _principalAccount = data;
    notifyListeners();
  }

  set titleView(String data) {
    _titleView = data;
    notifyListeners();
  }

  set idAccountToTransfer(String data) {
    _idAccountToTransfer = data;
    notifyListeners();
  }

  set accountToTransfer(AccountModel data) {
    _accountToTransfer = data;
    notifyListeners();
  }

  set isSaving(bool data) {
    _isSaving = data;
    notifyListeners();
  }

  Future<void> getListAccountSavingProvider(List<DocumentSnapshot> documentSnapshot) async {
    try {
      await getListAccount(documentSnapshot);
    } catch (e) {
      print(e);
    }
  }

  Future<void> getListAccountCurrentProvider(List<DocumentSnapshot> documentSnapshot) async {
    try {
      await getListAccount(documentSnapshot);
    } catch (e) {
      print(e);
    }
  }

  Future<void> getListAccount(List<DocumentSnapshot> documentSnapshot) async {
    listAccountModel = documentSnapshot.map((DocumentSnapshot e) {
      Map<String, dynamic> data = parseObjectToMap(e.data());
      return AccountModel.fromJson(data, e.id);
    }).toList();
    print(listAccountModel);
  }

  void handledButtonQR(BuildContext context, AccountModel accountModelParams) {
    accountModel = accountModelParams;
    openAlertDialog(
        context: context,
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TitleApp(label: "Actividades QR"),
              SizedBox(height: 10),
              TextApp(
                  label:
                      "Para facilitar tus transacciones puedes girar y recibir dinero por medio de un QR")
            ],
          ),
        ));
  }

  void _generateQR(BuildContext context) {
    openAlertDialog(context: context, body: GenerateQR(), activeButton: false);
  }

  Future<void> _openScan(BuildContext context) async {
    idAccountToTransfer = await scanner.scan();
    if (idAccountToTransfer.isNotEmpty)
      Navigator.of(context).pushNamed('/transfer');
  }

  void openAlertDialog({
    required BuildContext context,
    required Widget body,
    bool activeButton = true,
  }) {
    Alert(
            context: context,
            title: "",
            content: body,
            buttons: activeButton
                ? [
                    DialogButton(
                        child: TextApp(label: "Generar QR"),
                        onPressed: () {
                          Navigator.pop(context);
                          _generateQR(context);
                        },
                        color: Colors.blueAccent[100]),
                    DialogButton(
                        child: TextApp(label: "Leer QR"),
                        onPressed: () {
                          Navigator.pop(context);
                          _openScan(context);
                        },
                        color: Colors.greenAccent[100])
                  ]
                : [])
        .show();
  }

  Future<AccountModel> getAccountByIdProvider(String idAccount) async {
    DocumentSnapshot documentSnapshot = await getAccountById(idAccount);
    Map<String, dynamic> data = parseObjectToMap(documentSnapshot.data());
    accountToTransfer = AccountModel.fromJson(data, documentSnapshot.id);
    return accountToTransfer;
  }

  Future<void> getPrincipalAccountProvider(List<DocumentSnapshot> documentSnapshot) async {
    Map<String, dynamic> data = parseObjectToMap(documentSnapshot[0].data());
    principalAccount = AccountModel.fromJson(data, documentSnapshot[0].id);
  }

  Future<void> transferMoneyProvider({
    required AccountModel accountSend,
    required AccountModel accountGet,
    required int value
  }) async {

    accountSend.amount = accountSend.amount - value;
    accountGet.amount = accountGet.amount + value;

    await transferMoney(accountSend.toJson(), accountSend.id);
    await transferMoney(accountGet.toJson(), accountGet.id);
  }

  @override
  void dispose() {
    super.dispose();
    listAccountModel = [];
  }
}
