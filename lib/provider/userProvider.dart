import 'package:flutter/material.dart';
import 'package:pruebas/model/userModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:pruebas/service/userServices.dart';
import 'package:pruebas/utils/Format.dart';

class UserProvider extends UserServices with ChangeNotifier {
  late UserModel _userData;
  late UserModel _userDataToTransfer;

  UserModel get userData => _userData;
  UserModel get userDataToTransfer => _userDataToTransfer;

  set userData(UserModel data) {
    _userData = data;
    notifyListeners();
  }
  set userDataToTransfer(UserModel data) {
    _userDataToTransfer = data;
    notifyListeners();
  }

  Future<void> login(String email, String password) async {
    notifyListeners();
  }

  Future<UserModel> getUserByIdProvider(String idUser) async {
    DocumentSnapshot documentSnapshot = await getUserById(idUser);
    Map<String, dynamic> data = parseObjectToMap(documentSnapshot.data());
    userDataToTransfer = UserModel.fromJson(data, documentSnapshot.id);
    return userDataToTransfer;
  }
}