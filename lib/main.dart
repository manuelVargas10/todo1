import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pruebas/page/login/login.dart';
import 'package:pruebas/provider/accountProvider.dart';
import 'package:pruebas/provider/loginPorvider.dart';
import 'package:pruebas/provider/userProvider.dart';
import 'package:pruebas/provider/weatherProvider.dart';
import 'package:pruebas/routes.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();


  runApp(
    EasyLocalization(
      child: MyApp(),
      saveLocale: true,
      path: 'assets/translations',
      fallbackLocale: Locale('en', 'US'),
      supportedLocales: [
        Locale('en', 'US'),
        Locale('es', 'ES'),
        Locale('es', 'CO')
      ],
    ),
  );
}

class MyApp extends StatefulWidget {
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => LoginProvider()),
        ChangeNotifierProvider(create: (_) => AccountProvider()),
        ChangeNotifierProvider(create: (_) => WeatherProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.red,
          scaffoldBackgroundColor: Colors.grey[200],
        ),
        routes: {
          '/': (BuildContext context) {
            return Login();
          },
          ...Routes.getRoutes(context: context)
        },
      ),
    );
  }
}
