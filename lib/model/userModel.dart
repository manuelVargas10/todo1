class UserModel {
  UserModel({
    this.name = '',
    this.cellphone = '',
    this.identification = '',
    this.email = '',
    required this.id
  });

  String name;
  String cellphone;
  String identification;
  String email;
  String id;

  factory UserModel.fromJson(Map<String, dynamic> json, String id) => UserModel(
    id: id,
    name: json["name"],
    cellphone: json["cellphone"],
    identification: json["identification"],
    email: json["email"],
  );


  Map<String, dynamic> toJson() => {
    "name": name,
    "cellphone": cellphone,
    "identification": identification,
    "email": email,
  };
}
