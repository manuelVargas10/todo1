class AccountModel {
  AccountModel(
      {required this.accountNumber,
      required this.accountType,
      this.amount = 0,
      required this.userId,
      required this.id,
      this.isPrincipal = false});

  String accountNumber;
  String accountType;
  int amount;
  String userId;
  bool isPrincipal;
  String id;

  factory AccountModel.fromJson(Map<String, dynamic> json, String id) =>
      AccountModel(
          id: id,
          accountNumber: json["accountNumber"],
          accountType: json["accountType"],
          amount: json["amount"],
          userId: json["userId"],
          isPrincipal: json["isPrincipal"]);

  Map<String, dynamic> toJson() => {
        "accountNumber": accountNumber,
        "accountType": accountType,
        "amount": amount,
        "userId": userId,
        "isPrincipal": isPrincipal
      };
}
