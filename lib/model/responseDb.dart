class ResponseDB {
  bool isSuccess;
  String message;
  dynamic result;

  ResponseDB({
    required this.isSuccess,
    required this.message,
    this.result,
  });

  dynamic get response {
    return result;
  }
}