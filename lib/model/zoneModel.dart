class ZoneModel {
  ZoneModel({
    required this.distance,
    required this.title,
    required this.locationType,
    required this.woeid,
    required this.lattLong,
  });

  int distance;
  String title;
  String locationType;
  int woeid;
  String lattLong;

  factory ZoneModel.fromJson(Map<String, dynamic> json) => ZoneModel(
    distance: json["distance"],
    title: json["title"],
    locationType: json["location_type"],
    woeid: json["woeid"],
    lattLong: json["latt_long"],
  );

  Map<String, dynamic> toJson() => {
    "distance": distance,
    "title": title,
    "location_type": locationType,
    "woeid": woeid,
    "latt_long": lattLong,
  };
}
