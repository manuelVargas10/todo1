import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:easy_localization/easy_localization.dart';

Future<dynamic> useAlertDialog({
  required BuildContext context,
  String title = '',
  String message = '',
}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      if (Platform.isIOS) {
        return CupertinoAlertDialog(
          title: Text(title).tr(),
          content: Text(message).tr(),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () {
                Navigator.of(context).pop('false');
              },
              child: Text('cancel').tr(),
            ),
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop('true'),
              child: Text('accept').tr(),
            )
          ],
        );
      } else {
        return AlertDialog(
          title: Text(title).tr(),
          content: Text(message).tr(),
          actions: <Widget>[

          ],
        );
      }
    },
  );
}