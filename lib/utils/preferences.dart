import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class PreferenceUser {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late SharedPreferences prefs;
  late String token;

  initPrefs() async {}

  Future getUser() async {
    prefs = await _prefs;
    String? data = prefs.getString('auth');
    if (data != null && data != '') {
      return json.decode(data);
    }
    return {};
  }

  Future<void> setUser(Map<String, dynamic> auth) async {
    prefs = await _prefs;
    String dataEncode = json.encode(auth);
    prefs.setString('auth', dataEncode);
  }

  Future<void> deleteUser() async {
    prefs = await _prefs;
    prefs.setString('auth', '');
  }

  void clear() {
    prefs.clear();
  }
}
