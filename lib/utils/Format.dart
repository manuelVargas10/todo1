import 'dart:convert';

Map<String, dynamic> parseObjectToMap(dynamic object) =>jsonDecode(jsonEncode(object));

String numberFormat(int x) {
  List<String> parts = x.toString().split('.');
  RegExp re = RegExp(r'\B(?=(\d{3})+(?!\d))');

  parts[0] = parts[0].replaceAll(re, '.');
  return "${parts.join(',')}\$";
}

String accountFormat(String x) {
  List<String> parts = x.toString().split('.');
  RegExp re = RegExp(r'\B(?=(\d{4})+(?!\d))');

  parts[0] = parts[0].replaceAll(re, '-');
  return parts.join(',');
}

String iconWeather(double weather){
  if(weather >= 0 && weather <= 10) return "🌨❄️";
  else if(weather >= 11 && weather <= 13) return "⛈🌩";
  else if(weather >= 14 && weather <= 15) return "🌧🌦";
  else if(weather >= 16 && weather <= 18) return "🌥🌤";
  else if(weather >= 19 && weather <= 22) return "🌤☀️";
  else if(weather >= 23 && weather <= 50) return "☀️🔥";
  else return "🤷🏻‍";
}
