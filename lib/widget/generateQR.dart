import 'package:pruebas/provider/accountProvider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class GenerateQR extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return QrImage(
      data: Provider.of<AccountProvider>(context).accountModel.id,
      version: QrVersions.auto,
      size: 320,
      gapless: true,
    );
  }
}
