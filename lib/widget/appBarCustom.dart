import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/widget/titleApp.dart';

class AppBarCustom extends StatelessWidget implements PreferredSizeWidget {

  final String label;

  const AppBarCustom({required this.label});

  @override
  Widget build(BuildContext context) {
    return new AppBar(
      backgroundColor: Colors.white,
      title: TitleApp(
        label: label,
      ),
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios, color: Colors.black),
        onPressed: ()=> Navigator.of(context).pop(),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(50.0);
}
