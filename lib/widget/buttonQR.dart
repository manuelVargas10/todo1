import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonQR extends StatelessWidget {

  final Function handledButton;

  const ButtonQR({required this.handledButton});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> handledButton(),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 2),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.blue[900],
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                  offset: Offset(3, 2),
                  color: Colors.black26,
                  spreadRadius: 1,
                  blurRadius: 7)
            ]
        ),
        child: Icon(Icons.qr_code_outlined, size: 40,color: Colors.white,),
      ),
    );
  }
}
