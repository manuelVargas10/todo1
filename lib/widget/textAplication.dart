import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextApp extends StatelessWidget {
  final String label;
  final double size;

  const TextApp({required this.label, this.size = 15});

  @override
  Widget build(BuildContext context) {
    return Text(label, style: TextStyle(fontSize: size, fontWeight: FontWeight.w300, color: Colors.grey[700]),);
  }
}
