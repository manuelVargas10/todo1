import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/widget/textAplication.dart';

class IconButtonWidget extends StatelessWidget {
  final String label;
  final IconData icon;
  final Function handledButton;

  IconButtonWidget(
      {required this.label, required this.icon, required this.handledButton});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => handledButton(),
      child: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Icon(icon),
            TextApp(
              label: label,
              size: 12,
            )
          ],
        ),
      ),
    );
  }
}
