import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/utils/constans.dart';

class Input extends StatelessWidget {
  final icon; //final Icon
  final bool readOnly;
  final onTap; //final Function
  final suffixIcon; //final Icon
  final String hintText;
  final bool obscureText;
  final suffixIconHandled; //final Function
  final TextEditingController controller;
  final Function(dynamic) validator;
  final iconData; //IconData

  Input(
      {this.icon,
      this.onTap,
      required this.hintText,
      this.readOnly = false,
      this.suffixIcon,
      required this.controller,
      this.obscureText = false,
      this.suffixIconHandled,
      required this.validator,
      this.iconData});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: (e) => validator(e),
      controller: controller,
      readOnly: readOnly,
      obscureText: obscureText,
      onTap: onTap,
      decoration: InputDecoration(
          prefixIcon: Icon(
            iconData,
            color: colorGrey,
          ),
          fillColor: Colors.white,
          filled: true,
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(8.0),
            ),
            borderSide: new BorderSide(
              color: Colors.transparent,
              width: 1.0,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Colors.black,
              width: 0.3
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
                color: Colors.grey,
                width: 0.3
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(color: Colors.red, width: 0.3),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(color: Colors.red, width: 0.3),
          ),
          labelText: hintText,
          labelStyle: new TextStyle(color: colorGrey, fontSize: 16.0)),
    );
  }
}
