import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TitleApp extends StatelessWidget {
  final String label;
  final double size;

  const TitleApp({required this.label, this.size = 20});

  @override
  Widget build(BuildContext context) {
    return Text(label,
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: size, color: Colors.black));
  }
}
