import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pruebas/utils/constans.dart';

class Button extends StatelessWidget {
  final String label;
  final Function handled;

  Button({required this.label, required this.handled});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => handled(),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15),
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(2, 3),
                  blurRadius: 2,
                  spreadRadius: 0)
            ],
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: colorGrey, width: 0.3)),
        child: Center(
          child: Text(
            label,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 17,
                color: Colors.black54),
          ),
        ),
      ),
    );
  }
}
