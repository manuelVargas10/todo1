import 'package:flutter/material.dart';
import 'package:pruebas/page/developData/developData.dart';
import 'package:pruebas/page/home/home.dart';
import 'package:pruebas/page/listCount/ListCount.dart';
import 'package:pruebas/page/login/login.dart';
import 'package:pruebas/page/splash/splash.dart';
import 'package:pruebas/page/transfer/transfer.dart';
import 'package:pruebas/page/weather/weather.dart';

class Routes {
  static getRoutes({required BuildContext context}) {
    return {
      '/': (BuildContext context) => Splash(),
      '/login': (BuildContext context) => Login(),
      '/home': (BuildContext context) => Home(),
      '/listCount': (BuildContext context) => ListCount(),
      '/transfer': (BuildContext context) => Transfer(),
      '/weather': (BuildContext context) => Weather(),
      '/developData' : (BuildContext context) => DevelopData(),
    };
  }
}


